﻿#region Copyright Notice
//-----------------------------------------------------------------------
// <copyright>
//   Copyright © 2019 MTS Systems Corporation.  All rights reserved.
//   This software is furnished under a license and may be used and 
//   copied only in accordance with the terms of such license
//   and with inclusion of the above copyright notice.
//   <author>
//     MTS Systems Corporation, RomanPetlitsky, 2/20/2019 12:38:39 PM
//   </author>
// </copyright>
//-----------------------------------------------------------------------
#endregion

using ParserDocs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CVManagerModel
{
    public class ParserModel
    {
        public void Export(string folder, string toJsonPath)
        {
            ParseDocument parser = new ParseDocument {ToJsonPath = toJsonPath};
            parser.ParseFiles(Directory.GetFiles(folder));
        }
    }
}
