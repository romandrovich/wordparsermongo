﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Wordprocessing;
using User;
using Table = DocumentFormat.OpenXml.Wordprocessing.Table;

namespace ParserDocs
{
    public class ParseDocument
    {
        private User.User user = null;

        /// <summary>
        /// Json path
        /// </summary>
        public string ToJsonPath { get; set; }

        /// <summary>
        /// Parse doc files
        /// </summary>
        /// <param name="files"></param>
        public void ParseFiles(params string [] files)
        {
            foreach (var file in files)
            {
                ParseFile(file);
            }
        }

        private void ParseFile(string fileName)
        {
            using (WordprocessingDocument doc = WordprocessingDocument.Open(fileName, true))
            {
                ParseDocFile(doc);
            }
        }

        private void AddUserNameAndFamilyName(Paragraph p)
        {
            var names = p.InnerText.Split(' ');
            user.FirstName = names[0];
            user.LastName = names[1];
        }

        private void AddTechnicalExpertise(IEnumerable<Table> tableChildren)
        {
            foreach (var table in tableChildren)
            {
                foreach (var row in table.ChildElements.OfType<TableRow>())
                {
                    var cells = row.ChildElements.OfType<TableCell>().ToList();
                    TableCell type = cells.First();
                    TableCell types = cells.Last();
                    string[] separator = new string[] { ",", ";" };
                    var listOfTypes = types.InnerText.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                    string [] languages = new string[listOfTypes.Length];
                    for(int i =0; i < listOfTypes.Length; i++)
                    {
                        languages[i] = listOfTypes[i].Trim();
                    }
                    var technicalExpertise = EnumExtensionMethods.GetFriendlyColorEnums(type.InnerText);
                    switch (technicalExpertise)
                    {
                        case TechnicalExpertise.ProgrammingLanguages:
                            user.ProgrammingLanguages = new List<string>();
                            user.ProgrammingLanguages.AddRange(languages);
                            break;
                        case TechnicalExpertise.OperatingSystems:
                            user.OperatingSystems = new List<string>();
                            user.OperatingSystems.AddRange(languages);
                            break;
                        case TechnicalExpertise.Databases:
                            user.Databases = new List<string>();
                            user.Databases.AddRange(languages);
                            break;
                        case TechnicalExpertise.Technologies:
                            user.Technologies = new List<string>();
                            user.Technologies.AddRange(languages);
                            break;
                        case TechnicalExpertise.ToolsAndTechnologies:
                            user.ToolsAndTechnologies = new List<string>();
                            user.ToolsAndTechnologies.AddRange(languages);
                            break;
                        case TechnicalExpertise.Other:
                            user.Other = new List<string>();
                            user.Other.AddRange(languages);
                            break;
                    }
                }
            }
        }

        private void AddProjects(IEnumerable<Paragraph> paragraphs)
        {
            foreach (var paragraph in paragraphs)
            {
                if (paragraph.InnerText.Contains("Project: ") || paragraph.InnerText.Contains("Training: "))
                {
                    var project = new UserProject
                    {
                        Name = paragraph.InnerText.Split(':')[1]
                            .Substring(0, paragraph.InnerText.Split(':')[1].IndexOf("(", StringComparison.Ordinal)).Trim()
                    };

                    var text = paragraph.InnerText.Trim().Split('(')[1];
                    string[] dateSeparator = new string[] { "–", "-" };
                    var dates = text.Substring(0, text.Length - 1).Trim().Split(dateSeparator, StringSplitOptions.RemoveEmptyEntries);
                    var startDate = dates[0].Trim();
                    if(DateTime.TryParse(startDate, new DateTimeFormatInfo(), DateTimeStyles.AssumeLocal,
                        out var resultStartDate))
                        project.StartProject = resultStartDate;
                    DateTime resultEndDate;
                    var endDate = dates[1].Trim();
                    if (endDate.Contains("Current"))
                    {
                        project.EndProject = new DateTime(1900,1,1);
                    }
                    else
                    {
                        if(DateTime.TryParse(endDate, new DateTimeFormatInfo(), DateTimeStyles.AssumeLocal,
                            out resultEndDate))
                            project.EndProject = resultEndDate;
                    }
                    
                    while (paragraph.NextSibling<Paragraph>() != null)
                    {
                        var position = FindNextElement(paragraph.NextSibling<Paragraph>());
                        var descr = FindDescription(position.NextSibling<Paragraph>());
                        project.Position = position.InnerText;
                        project.Description = descr;
                        var languages = FindLanguagesAndTechnologies(position.NextSibling<Paragraph>());
                        if (languages == null) break;
                        string[] separator = new string[] { ",", ";" };
                        var listOfTypes = languages.InnerText.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                        string[] types = new string[listOfTypes.Length];
                        for (int i = 0; i < listOfTypes.Length; i++)
                        {
                            types[i] = listOfTypes[i].Trim();
                        }
                        project.LanguagesAndTechnologies = new List<string>();
                        types[0] = types[0].Split(':')[1].Trim();
                        project.LanguagesAndTechnologies.AddRange(types);
                        if (languages.InnerText.Contains("Languages and Technologie"))
                            break;
                    }

                    while (paragraph.NextSibling<Paragraph>() != null)
                    {
                        var position = FindNextElement(paragraph.NextSibling<Paragraph>());
                        var languages = FindToolsAndEnvironment(position.NextSibling<Paragraph>());
                        if (languages == null) break;
                        string[] separator = new string[] { ",", ";" };
                        var listOfTypes = languages.InnerText.Split(separator, StringSplitOptions.RemoveEmptyEntries);
                        string[] types = new string[listOfTypes.Length];
                        for (int i = 0; i < listOfTypes.Length; i++)
                        {
                            types[i] = listOfTypes[i].Trim();
                        }
                        project.ToolsAndEnvironment = new List<string>();
                        types[0] = types[0].Split(':')[1].Trim();
                        project.ToolsAndEnvironment.AddRange(types);
                        if (languages.InnerText.Contains("Tools and environment"))
                            break;
                    }

                    user.Projects.Add(project);
                    
                }
            }
        }

        /// <summary>
        /// Parse docs file
        /// </summary>
        /// <param name="doc"></param>
        private void ParseDocFile(WordprocessingDocument doc)
        {
            var paragraphs = doc.MainDocumentPart.Document.Body.Elements<Paragraph>();

            user = new User.User
            {
                DocumentCreated = doc.PackageProperties.Created,
                DocumentModified = doc.PackageProperties.Modified
            };
            AddUserNameAndFamilyName(paragraphs.First());
            AddTechnicalExpertise(doc.MainDocumentPart.Document.Body.Elements<Table>());
            AddProjects(paragraphs);


            user.SerializeUserToJson(ToJsonPath);
        }

        #region Helper Methods
        private Paragraph FindNextElement(Paragraph p)
        {
            if (p == null) return null;
            if (!string.IsNullOrEmpty(p.InnerText))
                return p;
            else
            {
               return FindNextElement(p.NextSibling<Paragraph>());
            }
        }

        private string FindDescription(Paragraph p)
        {
            StringBuilder sb = new StringBuilder();
            
            var paragraph = FindNextElement(p);
            if (paragraph == null) return string.Empty;
            if (!paragraph.InnerText.Contains("Languages and Technologies: "))
            {
                if (!string.IsNullOrEmpty(paragraph.InnerText))
                {
                    sb.Append(paragraph.InnerText);
                    sb.Append("\n");
                }

                sb.Append(FindDescription(paragraph.NextSibling<Paragraph>()));
            }
            

            return sb.ToString();
        }

        private Paragraph FindLanguagesAndTechnologies(Paragraph p)
        {
            var paragraph = FindNextElement(p);
            if (paragraph == null) return null;
            if (paragraph.InnerText.Contains("Languages and Technologie"))
            {
               return paragraph;
            }
           
            return FindLanguagesAndTechnologies(paragraph.NextSibling<Paragraph>());
        }

        private Paragraph FindToolsAndEnvironment(Paragraph p)
        {
            var paragraph = FindNextElement(p);
            if (paragraph == null) return null;
            if (paragraph.InnerText.Contains("Tools and environment"))
            {
                return paragraph;
            }

            return FindToolsAndEnvironment(paragraph.NextSibling<Paragraph>());
        }
        
        #endregion

    }
}
