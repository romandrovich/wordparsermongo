﻿#region Copyright Notice
//-----------------------------------------------------------------------
// <copyright>
//   Copyright © 2019 MTS Systems Corporation.  All rights reserved.
//   This software is furnished under a license and may be used and 
//   copied only in accordance with the terms of such license
//   and with inclusion of the above copyright notice.
//   <author>
//     MTS Systems Corporation, IvanBalyko, 2/14/2019 1:53:10 PM
//   </author>
// </copyright>
//-----------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace User
{
    [Serializable]
    public class UserProject
    {
        /// <summary>
        /// Name of project.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description of project.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Started to work on project.
        /// </summary>
        public DateTime? StartProject { get; set; }

        /// <summary>
        /// End project for the user.
        /// </summary>
        public DateTime? EndProject { get; set; }

        /// <summary>
        /// Position on the project.
        /// </summary>
        public string Position { get; set; }

        /// <summary>
        /// Used technologies on the project.
        /// </summary>
        public List<string> LanguagesAndTechnologies { get; set; }

        /// <summary>
        /// Used technologies on the project.
        /// </summary>
        public List<string> ToolsAndEnvironment { get; set; }
    }
}
