﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace User
{
    [Serializable]
    public class User
    {
        public User()
        {
            Projects = new List<UserProject>();
        }

        /// <summary>
        /// Get/Set when document is created.
        /// </summary>
        public DateTime? DocumentCreated { get; set; }

        /// <summary>
        /// Get/Set when document is modified.
        /// </summary>
        public DateTime? DocumentModified { get; set; }

        /// <summary>
        /// First Name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Last Name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Programming Languages
        /// </summary>
        public List<string> ProgrammingLanguages { get; set; }

        /// <summary>
        /// Databases
        /// </summary>
        public List<string> Databases { get; set; }

        /// <summary>
        /// Technologies
        /// </summary>
        public List<string> Technologies { get; set; }

        /// <summary>
        /// Tools And Technologies
        /// </summary>
        public List<string> ToolsAndTechnologies { get; set; }

        /// <summary>
        /// Operating Systems
        /// </summary>
        public List<string> OperatingSystems { get; set; }

        /// <summary>
        /// Other
        /// </summary>
        public List<string> Other { get; set; }

        /// <summary>
        /// User projectsy
        /// </summary>
        public List<UserProject> Projects { get; set; }


        public void SerializeUserToJson(string toJsonPath)
        {
            JsonSerializer serializer = new JsonSerializer();
            serializer.NullValueHandling = NullValueHandling.Ignore;

            using (StreamWriter sw = new StreamWriter(@toJsonPath + FirstName + "_" + LastName + ".json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, this);
            }
        }
    }
}
