﻿#region Copyright Notice
//-----------------------------------------------------------------------
// <copyright>
//   Copyright © 2019 MTS Systems Corporation.  All rights reserved.
//   This software is furnished under a license and may be used and 
//   copied only in accordance with the terms of such license
//   and with inclusion of the above copyright notice.
//   <author>
//     MTS Systems Corporation, IvanBalyko, 2/14/2019 1:34:41 PM
//   </author>
// </copyright>
//-----------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace User
{
    public enum TechnicalExpertise
    {
        [Description("Programming Languages")]
        ProgrammingLanguages,
        [Description("Databases")]
        Databases,
        [Description("Technologies")]
        Technologies,
        [Description("Tools and Technologies")]
        ToolsAndTechnologies,
        [Description("Operating Systems")]
        OperatingSystems,
        [Description("Other")]
        Other,
        [Description("None")]
        None
    }


    public static class EnumExtensionMethods
    {
        public static string GetDescription(this Enum GenericEnum)
        {
            Type genericEnumType = GenericEnum.GetType();
            MemberInfo[] memberInfo = genericEnumType.GetMember(GenericEnum.ToString());
            if ((memberInfo != null && memberInfo.Length > 0))
            {
                var _Attribs = memberInfo[0].GetCustomAttributes(typeof(System.ComponentModel.DescriptionAttribute), false);
                if ((_Attribs != null && _Attribs.Count() > 0))
                {
                    return ((System.ComponentModel.DescriptionAttribute)_Attribs.ElementAt(0)).Description;
                }
            }
            return GenericEnum.ToString();
        }


        public static TechnicalExpertise GetFriendlyColorEnums(string type)
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (TechnicalExpertise colorEnum in Enum.GetValues(typeof(TechnicalExpertise)))
            {
                if (colorEnum.GetDescription().Equals(type, StringComparison.OrdinalIgnoreCase))
                {
                    return colorEnum;
                }
            }
            return TechnicalExpertise.None;
        }
    }
}
