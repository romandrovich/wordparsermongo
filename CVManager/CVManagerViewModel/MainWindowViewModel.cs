﻿using System.Configuration;
using Prism.Commands;
using CommonServiceLocator;
using CVManagerModel;

namespace CVManagerViewModel
{
    public class MainWindowViewModel
    {
        ParserModel model = new ParserModel();

        public DelegateCommand ExportCommand { get;  }

        public MainWindowViewModel()
        {
            ExportCommand = new DelegateCommand(Export);
        }

        private  void Export()
        {        
            var dialogManager = ServiceLocator.Current.GetInstance<IDialogService>();
            var folder = dialogManager.ShowSelectFolderDlg();
            if (!string.IsNullOrEmpty(folder))
            {
                string toJsonPath = ConfigurationManager.AppSettings["ToJsonFiles"];
                model.Export(folder, toJsonPath);
            }
        }
    }
}
