﻿#region Copyright Notice
//-----------------------------------------------------------------------
// <copyright>
//   Copyright © 2019 MTS Systems Corporation.  All rights reserved.
//   This software is furnished under a license and may be used and 
//   copied only in accordance with the terms of such license
//   and with inclusion of the above copyright notice.
//   <author>
//     MTS Systems Corporation, RomanPetlitsky, 2/20/2019 11:31:58 AM
//   </author>
// </copyright>
//-----------------------------------------------------------------------
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CVManagerModel;
using Microsoft.Win32;

namespace CVManager.Services
{
    public class DialogService : IDialogService
    {
        public string ShowSelectFolderDlg()
        {
            var dlg = new FolderBrowserDialog();
            if (dlg.ShowDialog() == DialogResult.OK)
                return dlg.SelectedPath;
            return string.Empty;
        }
    }
}
