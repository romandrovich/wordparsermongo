﻿
using System.Windows;
using CommonServiceLocator;
using CVManager.Services;
using CVManagerModel;
using CVManagerViewModel;
using Prism.Unity;
using Unity;
using Unity.Lifetime;

namespace CVManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainWindowViewModel();
            ConfigureServiceContainer();
        }

        private static void ConfigureServiceContainer()
        {
            if (!ServiceLocator.IsLocationProviderSet)
            {
                var container = new UnityContainer();

                container.RegisterType<IDialogService, DialogService>(new ExternallyControlledLifetimeManager());

                ServiceLocator.SetLocatorProvider(() => new UnityServiceLocatorAdapter(container));
            }
        }
    }
}
